//! A simple library use the livetracking api

use reqwest;
use url::Url;
use serde::Deserialize;

const API_URL: &str = "https://hgslt-provide.azurewebsites.net/";

 #[derive(Deserialize)]
 #[derive(Debug)]
pub struct Datapoint{
    pub timestamp: u32,
    pub lat: f64,
    pub lon: f64,
    pub altitude: u32,
    pub speed: u32,
    pub unknown1: u32,
    pub unknown2: f64,
}

// Deserialize is a macro enabled in Cargo.toml
#[derive(Deserialize)]
#[derive(Debug)]
pub struct Data {
    pub maxGPSTime: u32,
    pub data: Vec<Datapoint>
}
 
 #[derive(Deserialize)]
 pub struct Tracking {
    pub data: Data,
 }

 #[derive(Deserialize)]
 pub struct RankingPilotData {
     pub id: String,
     pub gps_time: u64,
     pub points: f64,
     pub lat: f64,
     pub lon: f64,
     pub alt: i64,
 }

 #[derive(Deserialize)]
 pub struct PersonInfo {
    pub first_name: String,
    pub last_name: String,
    pub nationality: String,

    // Optional not all elements must be declared
    //pub gender: String,
    //pub facebook: Option<String>,
    //pub website: String,
    //pub tracker: String,
 }

#[derive(Deserialize)]
pub struct ParticipantsInfo {
    pub id: u32,
    pub athlete: PersonInfo,
    //pub supporter: Option<PersonInfo>,
    pub paid: bool,
    pub waiting_list: bool,
    pub wild_card: bool,
    pub canceled: bool,
    pub signed_in: bool,
    pub glider_brand: String,
    pub glider_model: String,
    pub glider_color: String,
    pub sponsor: String,
    pub rookie: bool,
    pub fun: bool,
    pub tandem: bool,
    pub unsupported: bool,
    //pub iphone: bool,
    //pub tracker: String,
    pub position: u32
}

 #[derive(Deserialize)]
 pub struct VendorData {
     pub start_time: String,
     pub goal_latitude: f64,
     pub goal_longitude: f64,
     pub goal_time: String,
     pub goal_radius: f64,
     pub flight_ratio: f64,
     pub goal_bonus: f64,
     pub minimum_distance: f64,
     pub published: bool,
     pub registration_open: String,
     pub registration_close: String,
     pub maximum_participants: u32,
     pub entry_fee: f64,
     pub canceled: bool,
     pub participants: Vec<ParticipantsInfo>,
 }


/// Returns the current track of a pilot with pilot_number pilot_number
/// # Examples
/// 
/// ## Fetch pilot tracking data
/// ```
/// let result = get_tracking("2022-garmisch", 1265, 10)
/// ```
pub fn get_tracking(race_name: &str, pilot_id: &str, resolution: u32) -> Result<Tracking, Box<dyn std::error::Error>> {
    let sub_url = format!("Reduced?race={}&team={}&res={}", race_name, pilot_id, resolution);
    return getTracking(&sub_url);
}

/// Returns the ranking of the provided race
/// Can be used to fetch all pilots and then using get_tracking() to fetch the track
/// 
/// # Examples
/// ```
/// let pilots = get_ranking("2022-garmisch");
/// ```
pub fn get_ranking(race_name: &str) -> Result<Vec<RankingPilotData>, Box<dyn std::error::Error>> {
    let sub_url = format!("Ranking?race={}", race_name);
    return getRanking(&sub_url);
}

///
/// let url = "http://bordairrace.live-tracking.com//data/2022-garmisch-vendor.json"
pub fn get_pilot_data(url: &str) -> Result<Vec<ParticipantsInfo>, Box<dyn std::error::Error>> {
    let vendor_data = getVendorData(url)?;
    return Ok(vendor_data.participants);
}

// Private section
//----------------------------------------------------------------------------------------------------

fn create_url(sub_url: &str) -> Result<Url, Box<dyn std::error::Error>> {
    let mut url;
    url = Url::parse(API_URL)?;
    url = url.join("api/")?;
    url = url.join(sub_url)?;
    println!("Url: {}", url);
    return Ok(url);
}

fn getVendorData(sub_url: &str) -> Result<VendorData, Box<dyn std::error::Error>> {
    let url = create_url(sub_url)?;

    // https://blog.logrocket.com/making-http-requests-rust-reqwest/
    let resp = reqwest::blocking::get(url.as_str())?;
    match resp.status() {
        reqwest::StatusCode::OK => {
            let element = resp.json::<VendorData>()?;
            return Ok(element)
        },
        _ => {
            return Err(format!("{}", resp.status().as_u16()).into());
        },
    };
}

fn getTracking(sub_url: &str) -> Result<Tracking, Box<dyn std::error::Error>> {
    let url = create_url(sub_url)?;

    // https://blog.logrocket.com/making-http-requests-rust-reqwest/
    let resp = reqwest::blocking::get(url.as_str())?;
    match resp.status() {
        reqwest::StatusCode::OK => {
            let element = resp.json::<Tracking>()?;
            return Ok(element)
        },
        _ => {
            return Err(format!("{}", resp.status().as_u16()).into());
        },
    };
}

fn getRanking(sub_url: &str) -> Result<Vec<RankingPilotData>, Box<dyn std::error::Error>> {
    let url = create_url(sub_url)?;

    // https://blog.logrocket.com/making-http-requests-rust-reqwest/
    let resp = reqwest::blocking::get(url.as_str())?;
    match resp.status() {
        reqwest::StatusCode::OK => {
            //let element = resp.json()?;
            return Ok(parseRanking(resp.json()?))
        },
        _ => {
            return Err(format!("{}", resp.status().as_u16()).into());
        },
    };
}

fn parseRanking(content: serde_json::Value) -> Vec<RankingPilotData> {
    let mut v = Vec::new();
    let result = content.as_object().unwrap();
    for key in result.keys() {
        if key.eq("race") {
            // this is not a valid pilot id
            continue;
        }

        println!("{:?}", key);
        let res = result.get(key).expect("").as_object().unwrap();
        let pilotData = RankingPilotData {
            id: key.to_string(),
            gps_time: res.get("gpsTime").unwrap().as_u64().unwrap(),
            points: res.get("points").unwrap().as_f64().unwrap(),
            lat: res.get("lat").unwrap().as_f64().unwrap(),
            lon: res.get("lon").unwrap().as_f64().unwrap(),
            alt: res.get("alt").unwrap().as_i64().unwrap(),
        };
        v.push(pilotData);
    }
    return v;
}

#[cfg(test)]
mod tests {

    use super::{Tracking, VendorData, parseRanking};
    
    #[test]
    fn test_parsing_json_pilot_tracking() {
        // This is the json content received from the api
        let contents = "
        [{\"maxGPSTime\":1654441172,
        \"data\":[
            [1654322782,47.4724483,11.0617383,756,5,167,1964.0],
            [1654322841,47.47183,11.0619533,758,5,159,338.0],
            [1654322901,47.47112,11.0621766,760,5,131,1819.0],
            [1654322961,47.4704383,11.062365,771,4,190,1746.0],
            [1654323021,47.469915,11.06262,786,3,120,1685.0],
            [1654323081,47.46981,11.063015,798,2,88,576.0],
            [1654323141,47.469515,11.0633916,806,3,138,1611.0],
            [1654323201,47.46919,11.06393,825,3,115,1555.0],
            [1654323260,47.46876,11.0644716,837,3,117,1489.0],
            [1654323320,47.4684666,11.064975,850,3,191,1438.0],
            [1654323380,47.46794,11.0647233,859,4,193,1401.0],
            [1654323440,47.4674533,11.0643233,867,3,210,856.0]
            ]}]
        ";
        
        let result: Tracking = serde_json::from_str(&contents).expect("");
        assert_eq!(result.data.maxGPSTime, 1654441172);
        assert_eq!(result.data.data.len(), 12);
        assert_eq!(result.data.data[0].timestamp, 1654322782);
        assert_eq!(result.data.data[0].lat, 47.4724483); 
        assert_eq!(result.data.data[0].lon, 11.0617383); 
        assert_eq!(result.data.data[0].altitude, 756); 
        assert_eq!(result.data.data[0].speed, 5);
        assert_eq!(result.data.data[0].unknown1, 167);
        assert_eq!(result.data.data[0].unknown2, 1964.0);
        
        assert_eq!(result.data.data[11].timestamp, 1654323440);
        assert_eq!(result.data.data[11].lat, 47.4674533); 
        assert_eq!(result.data.data[11].lon, 11.0643233); 
        assert_eq!(result.data.data[11].altitude, 867); 
        assert_eq!(result.data.data[11].speed, 3);
        assert_eq!(result.data.data[11].unknown1, 210);
        assert_eq!(result.data.data[11].unknown2, 856.0);
    }

    #[test]
    fn test_parsing_json_ranking() {
        let contents = "
        {\"race\":\"2022-garmisch\",
            \"1171\":{\"gpsTime\":1654349714,\"points\":16747.0,\"lat\":47.48532,\"lon\":11.07028,\"alt\":722},
            \"1173\":{\"gpsTime\":1654440981,\"points\":37854.0,\"lat\":47.480285,\"lon\":11.06449,\"alt\":723}}
        ";
        let result: serde_json::Value = serde_json::from_str(&contents).expect("");

        let res = parseRanking(result);
        assert_eq!(res.len(), 2);
        assert_eq!(res[0].id, "1171");
        assert_eq!(res[0].gps_time, 1654349714);
        assert_eq!(res[0].points, 16747.0);
        assert_eq!(res[0].lat, 47.48532);
        assert_eq!(res[0].lon, 11.07028);
        assert_eq!(res[0].alt, 722);
        assert_eq!(res[1].id, "1173");
        assert_eq!(res[1].gps_time, 1654440981);
        assert_eq!(res[1].points, 37854.0);
        assert_eq!(res[1].lat, 47.480285);
        assert_eq!(res[1].lon, 11.06449);
        assert_eq!(res[1].alt, 723);
    }

    #[test]
    fn test_fetch_pilot_names() {
        let content = "
        {
            \"id\":14,
            \"title\":\"Garmisch-Partenkirchen\",
            \"document\":null,
            \"start_latitude\":47.474618,
            \"start_longitude\":11.060152,
            \"start_time\":\"2022-06-04T06:00:00Z\",
            \"goal_latitude\":47.474618,
            \"goal_longitude\":11.060152,
            \"goal_time\":\"2022-06-05T15:00:00Z\",
            \"goal_radius\":0.2,
            \"flight_ratio\":0.2,
            \"goal_bonus\":0.2,
            \"minimum_distance\":15.0,
            \"published\":false,
            \"registration_open\":\"2022-05-12T22:00:00Z\",
            \"registration_close\":\"2022-05-26T21:45:00Z\",
            \"maximum_participants\":80,
            \"entry_fee\":79.0,
            \"canceled\":false,
            \"participants\":[
                {
                    \"id\":1163,
                    \"athlete\":{
                        \"first_name\":\"Willi\",
                        \"last_name\":\"Ludwig\",
                        \"nationality\":\"AUT\",
                        \"gender\":\"m\",
                        \"facebook\":null,
                        \"website\":\"http://www.paraglidingmoments.com/\",
                        \"tracker\":\"https://share.findmespot.com/shared/faces/viewspots.jsp?glId=0700BqyXLxpqEKLdkqIS4KAWdgg3TTApF\"
                    },
                    \"supporter\":null,
                    \"paid\":true,
                    \"waiting_list\":false,
                    \"wild_card\":true,
                    \"canceled\":false,
                    \"signed_in\":false,
                    \"glider_brand\":\"ADVANCE\",
                    \"glider_model\":\"Advance X- Alps\",
                    \"glider_color\":\"weiß\",
                    \"sponsor\":\"\",
                    \"rookie\":false,
                    \"fun\":false,
                    \"tandem\":false,
                    \"unsupported\":false,
                    \"iphone\":false,
                    \"tracker\":\"01964\",
                    \"position\":1
                }
            ],
            \"admin\":false
        }
        ";

        let result: VendorData = serde_json::from_str(&content).expect("");
    }
}
